<?php

/**
 * Places2Be Position.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Position\Tests;

use Generator;
use JsonException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Places2Be\Position\Coordinates;
use Places2Be\Position\Exception\InvalidInput;

/**
 * Class CoordinatesTest.
 */
class CoordinatesTest extends TestCase
{
    /**
     * @throws InvalidInput
     */
    #[DataProvider('getCoordinates')]
    public function testThrowsException(float $latitude, float $longitude): void
    {
        $this->expectException(InvalidInput::class);
        new Coordinates($latitude, $longitude);
    }

    /**
     * @return Generator<array<int, int>>
     */
    public static function getCoordinates(): Generator
    {
        yield [-91, 0];
        yield [91, 0];
        yield [0, -181];
        yield [0, 181];
    }

    /**
     * @throws InvalidInput
     * @throws JsonException
     */
    public function testCanSerialize(): void
    {
        $coordinates = new Coordinates(12.3456, 78.9123);

        self::assertSame(
            '{"latitude":12.3456,"longitude":78.9123}',
            json_encode($coordinates, JSON_THROW_ON_ERROR)
        );
    }
}
