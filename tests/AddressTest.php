<?php

/**
 * Places2Be Position.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Position\Tests;

use JsonException;
use PHPUnit\Framework\TestCase;
use Places2Be\Position\Address;

/**
 * Class AddressTest.
 */
class AddressTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testAddress(): void
    {
        $addressOriginal = [
            'streetName' => 'Überkinger Straße',
            'streetNumber' => '4',
            'zip' => '70372',
            'city' => 'Stuttgart',
            'country' => 'Deutschland',
        ];
        
        $addressObject = new Address(
            $addressOriginal['streetName'],
            $addressOriginal['streetNumber'],
            $addressOriginal['zip'],
            $addressOriginal['city'],
            $addressOriginal['country']
        );
                
        self::assertSame(
            $addressOriginal['streetName'],
            $addressObject->getStreetName()
        );

        self::assertSame(
            $addressOriginal['streetNumber'],
            $addressObject->getStreetNumber()
        );

        self::assertSame(
            $addressOriginal['zip'],
            $addressObject->getZip()
        );

        self::assertSame(
            $addressOriginal['city'],
            $addressObject->getCity()
        );

        self::assertSame(
            $addressOriginal['country'],
            $addressObject->getCountry()
        );
        
        self::assertSame(
            '{"streetName":"\u00dcberkinger Stra\u00dfe","streetNumber":"4","zip":"70372","city":"Stuttgart","country":"Deutschland","state":null}',
            json_encode($addressObject, JSON_THROW_ON_ERROR)
        );
    }
}
