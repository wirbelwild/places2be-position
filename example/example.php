<?php

use Places2Be\Position\Address;
use Places2Be\Position\Coordinates;

require '../vendor/autoload.php';

$address = new Address('Überkinger Straße', '4', '70372', 'Stuttgart', 'Deutschland');

var_dump($address);

$coordinates = new Coordinates(48.806032, 9.213567);

var_dump($coordinates);
