# Changes in Places2be Position 1.5

## 1.5.0 2021-03-30

### Added

-   The classes `Address` and `Coordinates` are now implementing `JsonSerializable`.