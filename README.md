[![PHP from Packagist](https://img.shields.io/packagist/php-v/places2be/position)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/places2be/position/v/stable)](https://packagist.org/packages/places2be/position)
[![Total Downloads](https://poser.pugx.org/places2be/position/downloads)](https://packagist.org/packages/places2be/position)
[![License](https://poser.pugx.org/places2be/position/license)](https://packagist.org/packages/places2be/position)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Places2Be Position

Handling addresses and coordinates.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/places2be/position). Add it to your project by running `$ composer require places2be/position`. 

## Usage 

Set up an address like that:

```php
<?php 

use Places2Be\Position\Address;

$address = new Address(
    'Überkinger Straße', 
    '4', 
    '70372', 
    'Stuttgart', 
    'Deutschland'
);
``` 

Set up coordinates like:

```php
<?php 

use Places2Be\Position\Coordinates;

$coordinates = new Coordinates(48.806032, 9.213567);
```

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).