<?php

/**
 * Places2Be Position.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Position;

use JsonSerializable;

/**
 * The Address class may handle an address.
 */
class Address implements JsonSerializable
{
    /**
     * Sets address.
     *
     * @param string $streetName   The street name.
     * @param string $streetNumber The street number.
     * @param string $zip          The ZIP code.
     * @param string $city         Name of the city.
     * @param string $country      Name of the country.
     * @param string|null $state   Name of the state.
     */
    public function __construct(
        private string $streetName,
        private string $streetNumber,
        private string $zip,
        private string $city,
        private string $country,
        private ?string $state = null
    ) {
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    public function getZip(): string
    {
        return $this->zip;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @return array<string, mixed>
     */
    public function jsonSerialize(): array
    {
        return [
            'streetName' => $this->getStreetName(),
            'streetNumber' => $this->getStreetNumber(),
            'zip' => $this->getZip(),
            'city' => $this->getCity(),
            'country' => $this->getCountry(),
            'state' => $this->getState(),
        ];
    }
}
