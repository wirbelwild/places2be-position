<?php

/**
 * Places2Be Position.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Position;

/**
 * Handles errors
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
