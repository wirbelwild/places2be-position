<?php

/**
 * Places2Be Position.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Position\Exception;

use Places2Be\Position\Exception;

/**
 * Handles invalid color systems errors
 */
class InvalidInput extends Exception
{
    /**
     * InvalidInput constructor.
     *
     * @param string $coordinates
     */
    public function __construct(string $coordinates)
    {
        parent::__construct('Input "' . $coordinates . '" is invalid');
    }
}
