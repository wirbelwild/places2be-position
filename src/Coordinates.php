<?php

/**
 * Places2Be Position.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Places2Be\Position;

use JsonSerializable;
use Places2Be\Position\Exception\InvalidInput;
use Stringable;

/**
 * The Coordinates class may handle coordinates.
 */
class Coordinates implements JsonSerializable, Stringable
{
    /**
     * Coordinates constructor.
     *
     * @throws InvalidInput
     */
    public function __construct(
        private float $latitude,
        private float $longitude
    ) {
        if ($latitude < -90 || $latitude > 90 || $longitude < -180 || $longitude > 180) {
            throw new InvalidInput($latitude . ',' . $longitude);
        }
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * Returns a string with the latitude and longitude.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getLatitude() . ',' . $this->getLongitude();
    }

    /**
     * @return array<string, float>
     */
    public function jsonSerialize(): array
    {
        return [
            'latitude' => $this->getLatitude(),
            'longitude' => $this->getLongitude(),
        ];
    }
}
